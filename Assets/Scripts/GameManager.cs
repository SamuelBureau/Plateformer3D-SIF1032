using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public int currentGold;
    public int maxGold;
    public TextMeshProUGUI m_text;
    private Object[] goldBars;

    // Start is called before the first frame update
    void Start()
    {
        goldBars = GameObject.FindGameObjectsWithTag("GoldBar");
        maxGold = goldBars.Length;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //  When player pickup gold, add gold to current gold
    public void AddGold(int goldToAdd)
    {
        currentGold += goldToAdd;
        m_text.text = "<sprite name=gold> " + currentGold.ToString();
    }
}
