﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public float moveSpeed;
    public float jumpForce;
    public CharacterController mycharacterController;
    private Vector3 moveDirection;
    public float gravityScale;

    public float knockbackForce;
    public float knockbackDuration;
    private float knockbackCounter;

    // Start is called before the first frame update
    void Start()
    {
        //myrigidbody = GetComponent<Rigidbody>();
        mycharacterController = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        //moveDirection = new Vector3(Input.GetAxis("Horizontal") * moveSpeed, moveDirection.y, Input.GetAxis("Vertical") * moveSpeed);
        if (knockbackCounter <= 0)
        {
            float yStore = moveDirection.y;
            moveDirection = (transform.forward * Input.GetAxisRaw("Vertical")) + (transform.right * Input.GetAxisRaw("Horizontal"));
            moveDirection = moveDirection.normalized * moveSpeed;
            moveDirection.y = yStore;

            if (mycharacterController.isGrounded)
            {
                moveDirection.y = 0f;

                if (Input.GetButtonDown("Jump"))
                {
                    moveDirection.y = jumpForce;
                }
            }
        }
        else
        {
            //If getting knocked back
            knockbackCounter -= Time.deltaTime;
        }

        moveDirection.y = moveDirection.y + (Physics.gravity.y * gravityScale * Time.deltaTime); //Adds gravity to player
        mycharacterController.Move(moveDirection * Time.deltaTime);
    }

    public void Knockback()
    {
        knockbackCounter = knockbackDuration;
    }
   
}
