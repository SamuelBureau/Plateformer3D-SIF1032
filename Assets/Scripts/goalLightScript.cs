using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class goalLightScript : MonoBehaviour
{
    Animator animator;
    LevelLoader levelLoader;

    public void Start()
    {
        animator = this.GetComponent<Animator>();
        levelLoader = GameObject.FindGameObjectWithTag("LevelLoader").GetComponent<LevelLoader>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            animator.SetTrigger("lightActivation");
            levelLoader.LoadNextLevel();
        }
    }
}
