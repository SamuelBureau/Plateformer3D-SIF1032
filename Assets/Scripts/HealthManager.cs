using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthManager : MonoBehaviour
{
    //Health datas
    public int maxHealth;
    public int currentHealth;
    public Image[] hearts;
    public Sprite fullHeart;
    public Sprite emptyHeart;

    public SimpleSampleCharacterControl thePlayer;

    //Invicibility datas
    public float invicibilityLength;
    private float invicibilityCounter;

    //Show damaga (flash renderer( datas
    public Renderer playerRenderer;
    private float flashCounter;
    public float flashLength = 0.1f;

    //Respawn datas
    private bool isRespawning;
    private Vector3 respawnPoint;
    public float respawnLength;

    //Death datas
    public GameObject deathEffect;
    public Image blackScreen;
    public Text youDiedText;
    private bool isFadeToBlack;
    private bool isFadeFromBlack;
    public float fadeSpeed;
    public float waitForFade;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;

        //Set respawn point to where the player starts
        respawnPoint = thePlayer.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < hearts.Length; i++)
        {
            //Show health
            if (i < currentHealth)
            {
                hearts[i].sprite = fullHeart;
            }
            else
            {
                hearts[i].sprite = emptyHeart;
            }

            //Always show 5 hearts
            /*if (i < maxHealth)
            {
                hearts[i].enabled = true;
            }
            else
            {
                hearts[i].enabled = false;
            }*/
        }

        if (invicibilityCounter > 0)
        {
            invicibilityCounter -= Time.deltaTime;

            flashCounter -= Time.deltaTime;
            if (flashCounter <= 0)
            {
                playerRenderer.enabled = !playerRenderer.enabled; //Flash renderer to show damage
                flashCounter = flashLength;
            }

            if (invicibilityCounter <= 0)
            {
                playerRenderer.enabled = true; //Reset renderer
            }
        }

        //Show blackscreen
        if (isFadeToBlack)
        {
            blackScreen.color = new Color(blackScreen.color.r, blackScreen.color.g, blackScreen.color.b, Mathf.MoveTowards(blackScreen.color.a, 1f, fadeSpeed * Time.deltaTime));
            youDiedText.color = new Color(youDiedText.color.r, youDiedText.color.g, youDiedText.color.b, Mathf.MoveTowards(youDiedText.color.a, 1f, fadeSpeed * Time.deltaTime));
            if (blackScreen.color.a == 1f)
            {
                isFadeToBlack = false;
            }
        }

        //Hide blackscreen
        if (isFadeFromBlack)
        {
            blackScreen.color = new Color(blackScreen.color.r, blackScreen.color.g, blackScreen.color.b, Mathf.MoveTowards(blackScreen.color.a, 0f, fadeSpeed * Time.deltaTime));
            youDiedText.color = new Color(youDiedText.color.r, youDiedText.color.g, youDiedText.color.b, Mathf.MoveTowards(youDiedText.color.a, 0f, fadeSpeed * Time.deltaTime));
            if (blackScreen.color.a == 0f)
            {
                isFadeFromBlack = false;
            }
        }
    }

    public void HurtPlayer(int damage, Vector3 direction)
    {
        //If player is not invicible, get damage
        if (invicibilityCounter <= 0) {
            currentHealth -= damage;

            //If dead, respawn. Else knockback and show damage
            if (currentHealth <= 0)
            {
                Respawn();
            }
            else
            {
                thePlayer.Knockback(direction);

                invicibilityCounter = invicibilityLength;

                //When gets damage, hide renderer
                playerRenderer.enabled = false;
                flashCounter = flashLength;
            }
        }
    }

    public void Respawn()
    {
        if (!isRespawning)
        {
            StartCoroutine("RespawnCo");
        }
    }

    //Co-routine for respawn
    public IEnumerator RespawnCo()
    {
        isRespawning = true;
        thePlayer.gameObject.SetActive(false);
        Instantiate(deathEffect, thePlayer.transform.position, thePlayer.transform.rotation);

        yield return new WaitForSeconds(respawnLength);

        isFadeToBlack = true;

        yield return new WaitForSeconds(waitForFade);

        isFadeToBlack = false;
        isFadeFromBlack = true;

        isRespawning = false;

        thePlayer.gameObject.SetActive(true);
        thePlayer.transform.position = respawnPoint;
        currentHealth = maxHealth;

        invicibilityCounter = invicibilityLength;
        playerRenderer.enabled = false;
        flashCounter = flashLength;
    }

    public void HealPlayer(int healAmount)
    {
        currentHealth += healAmount;

        if (currentHealth > maxHealth)
            currentHealth = maxHealth;
    }
}
