using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    public Animator transition;
    public float waitForFade = 2f;
    public float transtionTime = 6f;
    public TextMeshProUGUI goldCounterText;

    // Update is called once per frame
    void Update()
    {
    }

    public void LoadNextLevel()
    {
        if (SceneManager.GetActiveScene().buildIndex == SceneManager.sceneCountInBuildSettings-1)
        {
            //Load next level. (buildIndex = File -> Build Setting, index on the right of the scene in the list)
            StartCoroutine(LoadLevel(0));
        }
        else
        {
            StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
        }
        
    }

    IEnumerator LoadLevel(int levelIndex)
    {
        goldCounterText.text = "You have collected " + FindObjectOfType<GameManager>().currentGold +  " / " + FindObjectOfType<GameManager>().maxGold + " <sprite name=gold>";
        yield return new WaitForSeconds(2f);

        transition.SetTrigger("End");

        yield return new WaitForSeconds(3f);

        transition.SetTrigger("Start");
        SceneManager.LoadScene(levelIndex);
        
    }
}
